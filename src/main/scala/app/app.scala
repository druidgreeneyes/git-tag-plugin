package app

import plinth.*
import plinth.syntax.*
import plinth.std.*

import cats.syntax.all.*

case class Plugin(
    tagName: String,
    targetRef: Option[String],
    sourceRepo: Option[String],
    dryRun: Option[Boolean]
)

object app extends Plinth[Plugin] {
  override def run(config: Plugin): Either[CLIError, String] =
    log.debug(s"Config: $config")

    val resolvedRef: String =
      config.targetRef |> orElse { git.head } |> orThrow

    val resolvedRepo: String =
      config.sourceRepo |> orElse { git.remote.getUrl("origin") } |> orThrow

    val resolvedTagName = config.tagName

    git.tag(resolvedTagName, resolvedRef) *> {
      config.dryRun match
        case Some(true) =>
          git.tag.delete(resolvedTagName) *> {
            Right(s"Success: $resolvedTagName")
          }
        case _ => git.push("origin", resolvedRef)
    }
}
