FROM druidgreeneyes/sbt:build-alpine as build

WORKDIR /build

COPY . .

RUN sbt "compile; nativeLink"

FROM druidgreeneyes/sbt:run-alpine as run

RUN apk add git

COPY --from=build \
    /build/target/scala-3.2.2/git-tag-plugin-out \
    /app

ENTRYPOINT ["/app"]
