# Changelog

## [This Release](https://gitlab.com/druidgreeneyes/git-tag-plugin/compare/v0.1.1...HEAD) (2023-04-25)

### Fixes

* **git:** use plinth-std tag delete op instead of calling`-d` directly
([615fc42](https://gitlab.com/druidgreeneyes/git-tag-plugin/commit/615fc42b5cb27b7800ae36dc8df8c2c95bd929f0))

### [v0.1.1](https://gitlab.com/druidgreeneyes/git-tag-plugin/compare/v0.1.0...v0.1.1) (2023-04-25)

#### Fixes

* **git:** cleanup tag if dryRun is true
([244bed1](https://gitlab.com/druidgreeneyes/git-tag-plugin/commit/244bed1312dc42eef9de2a4fc5aa75eb79616750))

## v0.1.0 (2023-04-25)

### Features

* add automated image push to gitlab registry
([c1f4d9f](https://gitlab.com/druidgreeneyes/git-tag-plugin/commit/c1f4d9f59bbbd8a936c292879cc0a4ce27ab4631))
* add automated image push to gitlab registry
([6acb018](https://gitlab.com/druidgreeneyes/git-tag-plugin/commit/6acb018cfb7e0358519bac58b3ef799e8e0ff9d5))
* first pass with actual logic
([b27de7f](https://gitlab.com/druidgreeneyes/git-tag-plugin/commit/b27de7f9a2feded1e00115b33c4615ae1a9d3eef))
* **Opening commit:** Initialize repository
([066f27d](https://gitlab.com/druidgreeneyes/git-tag-plugin/commit/066f27d89d209f197111e32a09dea984078610db))

### Fixes

* **build:** correct project name from `root` to `git-tag-plugin`
([26728d3](https://gitlab.com/druidgreeneyes/git-tag-plugin/commit/26728d352d6237abd1500be0ed29f65e341bd0bc))
* **build:** fix release vs. snapshot version resolution
([ca143e4](https://gitlab.com/druidgreeneyes/git-tag-plugin/commit/ca143e495d213d62faf9b01a122cf381119945f1))
