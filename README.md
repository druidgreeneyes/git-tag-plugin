# Git Tag Plugin

CI tool to create and push a git tag based on environment variables.

in gitlab-ci, for example:

```yml
tag-version:
    image: registry.gitlab.com/druidgreeneyes/git-tag-plugin:0.1.2
    variables:
        PLUGIN_TAG_NAME: some-tag
        PLUGIN_DRY_RUN: true
```

Built using [plinth](https://gitlab.com/druidgreeneyes/plinth)
