val scala3Version = "3.2.2"

val mainBranch = "master"

def projectVersion: String = {
  val ver = IO.read(new File("version.txt")).trim()
  val ref = IO.read(new File(".git/HEAD"))
  val main = IO.read(new File(s".git/refs/remotes/origin/$mainBranch"))
  ref == main match {
    case true  => ver
    case false => ver + "-SNAPSHOT"
  }
}

lazy val plinthPath = file("../plinth")

lazy val plinth = RootProject(plinthPath)

import scala.scalanative.build.*

lazy val `git-tag-plugin` = (project in file("."))
  .enablePlugins(ScalaNativePlugin, GitlabPlugin)
  .settings(
    scalaVersion := scala3Version,
    version := projectVersion,
    nativeConfig ~= { c =>
      c.withLTO(LTO.none) // thin
        .withMode(Mode.debug) // releaseFast
        .withGC(GC.immix) // commix
    },
    gitlabRepositories ++= Seq(
      GitlabProjectRepository(gitlabDomain.value, GitlabProjectId("45185655"))
    )
  )
  .configure(p => {
    if (plinthPath.exists()) {
      p.dependsOn(plinth)
    } else {
      p.settings(
        libraryDependencies += "io.gitlab.druidgreeneyes" %%% "plinth" % "0.1.5"
      )
    }
  })
